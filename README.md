# meeting-cost-calculator


## Goal
This is a Single Page Application. Its goal is to calculate the estimated cost of a meeting.
It works by inputting the average annual salary of participants, the number of participants, the duration of the meeting and it’s frequency (weekly, monthly, ...).

## Prerequisites
- Git
- Node.js 16
- npm 7
- Docker optionnaly

## Quick start

### On the host machine
```bash
git clone https://gitlab.com/mthpvg/meeting-cost-calculator.git
cd meeting-cost-calculator
npm install
npm start
```
Visit: http://localhost:1234/

### Through Docker
```bash
git clone https://gitlab.com/mthpvg/meeting-cost-calculator.git
cd meeting-cost-calculator
docker build -t mthpvg/mcc .
docker run -p 1234:1234 mthpvg/mcc
```
Visit: http://localhost:1234/

## Tech stack
- React
- Parcel
- Tailwind CSS