FROM node:16-alpine3.13

RUN mkdir /opt/cloud && chown node:node /opt/cloud
WORKDIR /opt/cloud

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 1234

CMD ["node", "./node_modules/parcel/lib/bin.js", "serve", "./src/index.html"]
