import 'tailwindcss/tailwind.css'

import React from 'react'
import { render } from 'react-dom'
import Shell from './Shell'

render(<Shell></Shell>, document.getElementById('root'))