import React from 'react'

export default function Form(props) {
  return (
    <div className="p-12 md:w-1/2 flex flex-col items-start">
      <p className="leading-relaxed mb-8">The yearly cost of this meeting is {props.cost}CHF</p>
    </div>
  )

}