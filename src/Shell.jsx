import React from 'react'

import Navbar from './components/Navbar'
import Form from './components/Form'
import Result from './components/Result'

const yearlyDaysFor = {
  'daily': 47 * 5,
  'weekly': 47,
  'monthly': 12
}

export default class Shell extends React.Component {

  constructor() {
    super()

    this.state = {
      participants: 3,
      salary: 50000,
      duration: 60,
      frequency: 'weekly',
      cost: 881250
    }

    this.handleParticipants = this.handleParticipants.bind(this);
    this.handleSalary = this.handleSalary.bind(this);
    this.handleDuration = this.handleDuration.bind(this);
    this.handleFrequency = this.handleFrequency.bind(this);
  }

  handleParticipants(event) {
    const participants = parseFloat(event.target.value)
    this.setState({participants}, this.compute)
  }

  handleSalary(event) {
    const salary = parseFloat(event.target.value)
    this.setState({salary}, this.compute)
  }

  handleDuration(event) {
    const duration = parseFloat(event.target.value)
    this.setState({duration}, this.compute)
  }

  handleFrequency(event) {
    const frequency = event.target.value
    this.setState({frequency}, this.compute)
  }

  compute() {
    const {participants, salary, duration, frequency} = this.state
    const days = yearlyDaysFor[frequency]
    const fractionOfADay = duration / (8 * 60)

    this.setState(
      {cost: Math.round(participants * salary * fractionOfADay * days)}
    )
  }

  render() {
    return (
    <div>
      <Navbar></Navbar>
      <section className="text-gray-600 body-font overflow-hidden">
        <div className="container px-5 py-24 mx-auto">
          <div className="flex flex-wrap -m-12">
            <Form
              handleParticipants={this.handleParticipants}
              participants={this.state.participants}
              handleSalary={this.handleSalary}
              salary={this.state.salary}
              handleDuration={this.handleDuration}
              duration={this.state.duration}
              handleFrequency={this.handleFrequency}
              frequency={this.state.frequency}
            >
            </Form>
            <Result cost={this.state.cost}></Result>
          </div>
        </div>
      </section>
    </div>
    )
  }

}